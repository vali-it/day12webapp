package ee.bcs.valiit.company;

import static org.junit.Assert.*;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import ee.bcs.valiit.company.dao.Company;
import personalcode.PersonalCodeAppTest;

public class CompanyServiceTest {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testPerformSqlSelect() throws SQLException {
		ClassLoader classLoader = CompanyServiceTest.class.getClassLoader();
		String path = classLoader.getResource("test1.txt").getPath();
		
		ResultSet result = CompanyService.performSqlSelect("select * from company");
		assertTrue(result.next());
	}

	@Test
	public void testGetCompanies() {
		Company[] companies = CompanyService.getCompanies();
		assertTrue(companies != null);
		assertTrue(companies.length > 0);
		assertTrue(companies[0].getId() > 0);
	}
	
}
