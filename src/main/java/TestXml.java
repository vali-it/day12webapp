import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;

@JsonRootName("ROOT_ELEMENT")
public class TestXml {

	private String name = "";
	private int someNumber = 0;
	
	@JsonProperty("SOME_NAME")
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getSomeNumber() {
		return someNumber;
	}
	public void setSomeNumber(int someNumber) {
		this.someNumber = someNumber;
	}
}
