package ee.bcs.valiit.web;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Map;

import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;

import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataParam;
import org.glassfish.jersey.server.mvc.Viewable;

@Path("/")
public class WebResource {

	@POST
	@Path("/upload")
	@Consumes({ MediaType.MULTIPART_FORM_DATA })
	public Viewable uploadPdfFile(@FormDataParam("file") InputStream fileInputStream,
			@FormDataParam("file") FormDataContentDisposition fileMetaData) throws Exception {
		String UPLOAD_PATH = "c:/tmp/20180319/";
		try {
			int read = 0;
			byte[] bytes = new byte[1024];
			OutputStream out = new FileOutputStream(new File(UPLOAD_PATH + fileMetaData.getFileName()));
			while ((read = fileInputStream.read(bytes)) != -1) {
				out.write(bytes, 0, read);
			}
			out.flush();
			out.close();
		} catch (IOException e) {
			throw new WebApplicationException("Error while uploading file. Please try again !!");
		}
		return new Viewable("/upload_complete.ftl");
	}

	@GET
	@Path("/contact/form")
	@Produces(MediaType.TEXT_HTML)
	public Viewable getHello() {
		Map<String, String> map = new HashMap<String, String>();
		map.put("testparam", "noheader");
		return new Viewable("/contact_form.ftl", map);
	}

	@POST
	@Path("/contact/view")
	@Produces(MediaType.TEXT_HTML)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	public Viewable setPurchase(@FormParam("first_name") String firstName, @FormParam("last_name") String lastName,
			@FormParam("phone") String phone, @FormParam("email") String email) {
		Map<String, String> map = new HashMap<String, String>();
		map.put("first_name", firstName);
		map.put("last_name", lastName);
		map.put("phone", phone);
		map.put("email", email);
		return new Viewable("/contact_view.ftl", map);
	}

	@GET
	@Path("/numbers/add")
	@Produces(MediaType.TEXT_HTML)
	public Viewable getNumbersAdd() {
		return new Viewable("/rest_demo.ftl");
	}
}
