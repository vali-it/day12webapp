package ee.bcs.valiit.rest;

import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.ws.rs.Consumes;
import javax.ws.rs.CookieParam;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.NewCookie;
import javax.ws.rs.core.Response;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataParam;
import org.glassfish.jersey.server.mvc.Viewable;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import ee.bcs.valiit.company.CompanyService;
import ee.bcs.valiit.company.dao.Company;
import ee.bcs.valiit.model.MessageDTO;

@Path("/")
public class RestResource {

	@POST
	@Path("/upload")
	@Consumes({ MediaType.MULTIPART_FORM_DATA })
	public String upload(@FormDataParam("testparam") String testParam, @FormDataParam("file") InputStream fileInputStream,
			@FormDataParam("file") FormDataContentDisposition fileMetaData) throws Exception {
		String UPLOAD_PATH = "c:/tmp/20180319/";
		try {
			int read = 0;
			byte[] bytes = new byte[1024];
			OutputStream out = new FileOutputStream(new File(UPLOAD_PATH + fileMetaData.getFileName()));
//			OutputStream out = new FileOutputStream(new File(UPLOAD_PATH + "test.jpg"));
			while ((read = fileInputStream.read(bytes)) != -1) {
				out.write(bytes, 0, read);
			}
			out.flush();
			out.close();
		} catch (IOException e) {
			throw new WebApplicationException("Error while uploading file. Please try again !!");
		}
		return testParam;
	}
	
	
	@GET
	@Path("/readxml")
	@Produces(MediaType.TEXT_PLAIN)
	public String readXml() throws SAXException, IOException, ParserConfigurationException {
		File fXmlFile=new File("c:/tmp/test.xml");
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
		Document doc = dBuilder.parse(fXmlFile);
		doc.getDocumentElement().normalize();
		NodeList nList = doc.getElementsByTagName("product");
		for(int i = 0; i < nList.getLength(); i++) {
			System.out.println(nList.item(0).getChildNodes().item(0).getNodeValue());
		}
		return "OK";
	}
	
	@GET
	@Path("/displayimage")
	@Produces("image/jpg")
	public Response getFullImage(@QueryParam("image_name") String imageName) throws IOException {

	    BufferedImage image = ImageIO.read(new File("C:/tmp/20180319/" + imageName));

	    ByteArrayOutputStream baos = new ByteArrayOutputStream();
	    ImageIO.write(image, "jpg", baos);
	    byte[] imageData = baos.toByteArray();

//	     uncomment line below to send non-streamed
	     return Response.ok(imageData).build();

	    // uncomment line below to send streamed
	    // return Response.ok(new ByteArrayInputStream(imageData)).build();
	}
	
	@GET
	@Path("/readexcel")
	@Produces(MediaType.TEXT_PLAIN)
	public String readExcel() {
        try {

            FileInputStream excelFile = new FileInputStream(new File("C:/tmp/excel.xls"));
            Workbook workbook = new HSSFWorkbook(excelFile);
            Sheet datatypeSheet = workbook.getSheetAt(0);
            Iterator<Row> iterator = datatypeSheet.iterator();
            
            while (iterator.hasNext()) {

                Row currentRow = iterator.next();
                Iterator<Cell> cellIterator = currentRow.iterator();

                while (cellIterator.hasNext()) {

                    Cell currentCell = cellIterator.next();
                    //getCellTypeEnum shown as deprecated for version 3.15
                    //getCellTypeEnum ill be renamed to getCellType starting from version 4.0
                    if (currentCell.getCellTypeEnum() == CellType.STRING) {
                        System.out.print(currentCell.getStringCellValue() + "--");
                    } else if (currentCell.getCellTypeEnum() == CellType.NUMERIC) {
                        System.out.print(currentCell.getNumericCellValue() + "--");
                    }

                }
                System.out.println();

            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
		return "OK";
	}
	
	@GET
	@Path("setcookiesample")
	@Produces(MediaType.TEXT_PLAIN)
	public String setCookieSample(@Context HttpServletRequest req) {
		
        HttpSession session= req.getSession(true);
        Object foo = session.getAttribute("user");
        if (foo!=null) {
            System.out.println(foo.toString());
        } else {
            foo = "bar";
            session.setAttribute("user", Math.random());
        }
        
        return "OK";
        
//		NewCookie valiitCookie = new NewCookie("valiittest", "ABC123", "/", null, null, 84600, false);
//		return Response.ok("Kypsis paigaldatud!").cookie(valiitCookie).build();
	}

	@GET
	@Path("/readcookiesample")
	@Produces(MediaType.TEXT_PLAIN)
	public String readCookieSample(@CookieParam("valiittest") String cookieValue) {
		return "The cookie was read by server. It had value " + cookieValue;
	}
	
	@GET
	@Path("/companies")
	@Produces(MediaType.APPLICATION_JSON)
	public Company[] getCompanies() {
		return CompanyService.getCompanies();
	}

	@POST
	@Path("/company")
	@Produces(MediaType.TEXT_PLAIN)
	@Consumes(MediaType.APPLICATION_JSON)
	public String saveCompany(@Context HttpServletRequest req, Company company) {
		CompanyService.saveCompany(company);
		return "OK";
	}

	@GET
	@Path("/hi/text")
	@Produces(MediaType.TEXT_PLAIN)
	public String getHi(@Context HttpServletRequest req, @DefaultValue("World") @QueryParam("name") String name) {
        HttpSession session= req.getSession(true);
        Object foo = session.getAttribute("foo");
        if (foo!=null) {
            System.out.println(foo.toString());
        } else {
            foo = "bar";
            session.setAttribute("foo", Math.random());
        }
		return String.format("Hello, %s!", name);
	}

	@GET
	@Path("/hi/json/map/{name}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getHi2(@PathParam(value = "name") String name) {
		Map<String, String> response = new HashMap<>();
		response.put("greeting", String.format("Hello, %s!", name));
		return Response.ok(response).build();
	}

	@GET
	@Path("/hi/json/dto")
	@Produces(MediaType.APPLICATION_JSON)
	public MessageDTO getHi3(@DefaultValue("World") @QueryParam("name") String name) {
		MessageDTO message = new MessageDTO();
		message.setText("Hello, " + name + "!!!");
		return message;
	}

	@POST
	@Path("/message")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.TEXT_PLAIN)
	public String message(MessageDTO message) {
		return "Sõnum oli järgmine: " + message.getText();
	}

	@POST
	@Path("/addnumbers")
	@Produces(MediaType.APPLICATION_JSON)
	public Response addNumbers(@FormParam("number1") String number1, @FormParam("number2") String number2) {
		int value1 = Integer.parseInt(number1);
		int value2 = Integer.parseInt(number2);
		int result = value1 + value2;
		Map<String, String> response = new HashMap<>();
		response.put("result", String.valueOf(result));
		response.put("comment", "See summa arvutati kokku RestResource klassis serveri pool.");
		return Response.ok(response).build();
	}

	@GET
	@Path("/message/db")
	@Produces(MediaType.TEXT_PLAIN)
	public String getTextFromDb() throws SQLException, ClassNotFoundException {
		// Class.forName("com.mysql.cj.jdbc.Driver");
		Class.forName("org.mariadb.jdbc.Driver");
		try (Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/valiit", "root", "tere")) {
			try (Statement stmt = conn.createStatement()) {
				try (ResultSet rs = stmt.executeQuery("SELECT simple_column FROM simpledemo")) {
					rs.first();
					String result = rs.getString(1);
					conn.close();
					return result;
				}
			}
		}
	}
}
