import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;

public class XmlSerializerTest {

	public static void main(String[] args) throws JsonProcessingException {
	    XmlMapper xmlMapper = new XmlMapper();
	    TestXml testXml = new TestXml();
	    testXml.setName("See on mingi test nimi");
	    testXml.setSomeNumber(34567);
		String xml = xmlMapper.writeValueAsString(testXml);
	    System.out.println(xml);
	}
	
}
